const { urlencoded } = require("body-parser");
const express = require("express");
let scores = require("./data/scores.json");
//becasue const can not be changed. Data that plans to be changed should be imported with a let variable.

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//this allows for the url to be analyzed as well.
//Doing this makes the URL safe to be transported.

app.get("/", (req, res) => {
  res.send("Hello from my server");
});

app.get("/scores", (req, res) => {
  res.status(200).send(scores);
});

app.get("/*", (req, res) => {
  res.status("404").send("Where are you going, man??");
});

app.post("/scores", (req, res) => {
  //the body is information that will be requested by client side
  //req.body shouldn't be used in a GET request like above. However posts are preffered to intercept incoming info.
  const newPlayer = req.body;
  scores.unshift(newPlayer);

  console.log(scores);

  res.status(201).send(scores);
});

//Keep ports within 3-9thousand
app.listen(3000, () => console.log("Server has started on port 3000"));
